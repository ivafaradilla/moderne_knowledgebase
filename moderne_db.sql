-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2020 at 03:19 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `knowledge_base`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `ID` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `canned_responses`
--

CREATE TABLE `canned_responses` (
  `ID` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('01salek9veuq5eb4046unfttv5mpj7hd', '::1', 1601644728, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313634343630363b),
('1jr2v64tu5l9vfg3vfl5sm52a9hm3qea', '::1', 1601621922, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632313932323b),
('4bksra4m02fbs5m9p63kk6lqu1rsd7u5', '::1', 1601530019, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533303031393b),
('4brkkdhrmmq2r8k6klhubochk68adbfi', '::1', 1601535554, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533353535343b),
('4dtn41t23tn73b6eeq0u71eaok53jaoa', '::1', 1601534387, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533343338373b),
('4fu15crbvr9dpaajj30krqj24lehjfb3', '::1', 1601623063, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632333036333b),
('4k9ivk84p5d5ajodlg4v4q962qlvuqmk', '::1', 1601534834, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533343833343b),
('4urdvkmdubhvdchvl6khs35uo065fs2c', '::1', 1601644606, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313634343630363b),
('5j083dri3qlco14mod9212islmdbedoh', '::1', 1601619916, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313631393931363b),
('5oaijmn792m5a44drpd1eguirn5sg4he', '::1', 1601619910, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313631393931303b),
('75bkujs4j7tle1vk5fhdijl3ejf7bq5l', '::1', 1601619064, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313631393036343b),
('75k5es5a3ti924794m7pf1mqrn2md5g4', '::1', 1601620592, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632303539323b),
('7g7ohlee616v76t9ha42urnqj3455vmg', '::1', 1601535980, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533353938303b),
('8g4o5lpg4c057g98fkvbvrjrhp97d4uf', '::1', 1601624362, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632343230303b),
('9784gnk8hfeer94itor70prvve5n1obr', '::1', 1601531581, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533313538313b),
('97cssovi2gvs4ckpe0co441o9h44984t', '::1', 1601535140, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533353134303b),
('a0ua0flkudd89i34fd60k169f2cfdab7', '::1', 1601619910, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313631393931303b),
('b44e944rqdgtmitdj7gs51bni560de09', '::1', 1601530949, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533303934393b),
('bso4m4rtm6kui7fd2pq3a5j3v5o766bn', '::1', 1601623755, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632333735353b),
('cd6iblgc3ai05rheij4vggha17c382if', '::1', 1601536105, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533353938303b),
('d2cll9rnp7mfenh7vn1bnlf44kleqk74', '::1', 1601530333, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533303333333b),
('f004nf2ctvi7e45l8fsdsf5dsctmli5e', '::1', 1601622245, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632323234353b),
('funh1numkilsgrae4rv9kn6h8itl2fl4', '::1', 1601533732, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533333733323b),
('g5vlrmvla9t72rgd0k48tmjpnt0r3p4c', '::1', 1601619468, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313631393436383b),
('jogn36emar3i3m2s6um4c3icasa0p42q', '::1', 1601531890, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533313839303b),
('rfc3ok16o36ppmo1burgq9o7abtdabuk', '::1', 1601624200, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632343230303b),
('s2i80vo4pu7r1od4j4cah0i3clnp0ng8', '::1', 1601620291, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632303239313b),
('s7mlb2nafmbrs5efclg8hd1pgpurkf3o', '::1', 1601621242, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632313234323b),
('u286ur5rmo6k8gk8tigp11ad0685dp2l', '::1', 1601622672, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632323637323b),
('uc1nn3k1atk24udv9ar5ru1j45umcf4q', '::1', 1601532445, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313533323434353b),
('vra4ormh8niolmd8in2tksah4amk3fae', '::1', 1601623422, 0x5f5f63695f6c6173745f726567656e65726174657c693a313630313632333432323b);

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `options` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `required` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `help_text` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `register` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_statuses`
--

CREATE TABLE `custom_statuses` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(6) NOT NULL,
  `close` int(11) NOT NULL,
  `text_color` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_statuses`
--

INSERT INTO `custom_statuses` (`ID`, `name`, `color`, `close`, `text_color`) VALUES
(1, 'New', '79d7ef', 0, 'FFFFFF'),
(2, 'In Progress', 'fab13e', 0, 'FFFFFF'),
(3, 'Closed', 'ec6060', 1, 'FFFFFF');

-- --------------------------------------------------------

--
-- Table structure for table `custom_views`
--

CREATE TABLE `custom_views` (
  `ID` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `order_by` int(11) NOT NULL,
  `order_by_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `documentation_projects`
--

CREATE TABLE `documentation_projects` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(500) NOT NULL,
  `footer` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `ID` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `document` text NOT NULL,
  `timestamp` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  `link_documentid` int(11) NOT NULL,
  `sort_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `document_files`
--

CREATE TABLE `document_files` (
  `ID` int(11) NOT NULL,
  `documentid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `extension` varchar(25) NOT NULL,
  `file_size` int(11) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `ID` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `hook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`ID`, `title`, `message`, `hook`, `language`) VALUES
(1, 'Forgot Your Password', '<p>Dear [NAME],<br />\r\n<br />\r\nSomeone (hopefully you) requested a password reset at [SITE_URL].<br />\r\n<br />\r\nTo reset your password, please follow the following link: [EMAIL_LINK]<br />\r\n<br />\r\nIf you did not reset your password, please kindly ignore this email.<br />\r\n<br />\r\nYours,<br />\r\n[SITE_NAME]</p>\r\n', 'forgot_password', 'english'),
(2, 'Email Activation', '<p>Dear [NAME],<br />\r\n<br />\r\nSomeone (hopefully you) has registered an account on [SITE_NAME] using this email address.<br />\r\n<br />\r\nPlease activate the account by following this link: [EMAIL_LINK]<br />\r\n<br />\r\nIf you did not register an account, please kindly ignore this email.<br />\r\n<br />\r\nYours,<br />\r\n[SITE_NAME]</p>\r\n', 'email_activation', 'english'),
(3, 'Support Ticket Reply', '<p>[IMAP_TICKET_REPLY_STRING]<br />\r\n<br />\r\nDear [NAME],<br />\r\n<br />\r\nA new reply was posted on your ticket:<br />\r\n<br />\r\n[TICKET_BODY]<br />\r\n<br />\r\nTicket was generated here: [SITE_URL]<br />\r\n<br />\r\n[IMAP_TICKET_ID] [TICKET_ID]<br />\r\n<br />\r\nYours,<br />\r\n[SITE_NAME]</p>\r\n', 'ticket_reply', 'english'),
(4, 'Support Ticket Creation', '<p>[IMAP_TICKET_REPLY_STRING]<br />\r\n<br />\r\nDear [NAME],<br />\r\n<br />\r\nThanks for creating a ticket at our site: [SITE_URL]<br />\r\n<br />\r\nYour message:<br />\r\n<br />\r\n[TICKET_BODY]<br />\r\n<br />\r\nWe&#39;ll be in touch shortly.<br />\r\n<br />\r\n[IMAP_TICKET_ID] [TICKET_ID]<br />\r\n<br />\r\nYours,<br />\r\n[SITE_NAME]</p>\r\n', 'ticket_creation', 'english'),
(5, 'Support Guest Ticket Creation', '<p>[IMAP_TICKET_REPLY_STRING]<br />\r\n<br />\r\nDear [NAME],<br />\r\n<br />\r\nThanks for creating a ticket at our site: [SITE_URL]<br />\r\n<br />\r\nYour message:<br />\r\n<br />\r\n[TICKET_BODY]<br />\r\n<br />\r\nTo view your ticket, use these Guest Login details:<br />\r\nEmail: [GUEST_EMAIL]<br />\r\nPassword: [GUEST_PASS]<br />\r\n<br />\r\nGuests Login Here: [GUEST_LOGIN]<br />\r\n<br />\r\nWe&#39;ll be in touch shortly.<br />\r\n<br />\r\n[IMAP_TICKET_ID] [TICKET_ID]<br />\r\n<br />\r\nYours,<br />\r\n[SITE_NAME]</p>\r\n', 'guest_ticket_creation', 'english'),
(7, 'Ticket Reminder', '<p>Dear [NAME],<br />\r\n<br />\r\nThis is a reminder that you currently have an open ticket that needs your attention.<br />\r\n<br />\r\nPlease login to your ticket at:<br />\r\n<br />\r\n[SITE_URL]<br />\r\n<br />\r\nEmail Login: [NAME]<br />\r\nTicket Password: [GUEST_PASS]<br />\r\n<br />\r\nYours,<br />\r\n[SITE_NAME]</p>\r\n', 'ticket_reminder', 'english'),
(8, 'New Notification', 'Dear [NAME],<br /><br />\r\n\r\nYou have a new notification waiting for you at [SITE_NAME]. You can view it by logging into:<br /><br />\r\n\r\n[SITE_URL]<br /><br />\r\n\r\nYours,<br />\r\n[SITE_NAME]', 'new_notification', 'english'),
(9, 'Close Ticket', '<p>Dear [NAME],</p>\r\n\r\n<p>We have just closed your ticket at [SITE_URL]. You can view the ticket here: [TICKET_URL].</p>\r\n\r\n<p>If you need anymore assistance, please open a new ticket and we&#39;ll be happy to help.</p>\r\n\r\n<p>Thanks,</p>\r\n\r\n<p>[SITE_NAME]</p>\r\n', 'close_ticket', 'english');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `ID` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `answer` varchar(2000) NOT NULL,
  `catid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `home_stats`
--

CREATE TABLE `home_stats` (
  `ID` int(11) NOT NULL,
  `google_members` int(11) NOT NULL DEFAULT 0,
  `facebook_members` int(11) NOT NULL DEFAULT 0,
  `twitter_members` int(11) NOT NULL DEFAULT 0,
  `total_members` int(11) NOT NULL DEFAULT 0,
  `new_members` int(11) NOT NULL DEFAULT 0,
  `active_today` int(11) NOT NULL DEFAULT 0,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `total_tickets` int(11) NOT NULL,
  `total_assigned_tickets` int(11) NOT NULL,
  `tickets_today` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_stats`
--

INSERT INTO `home_stats` (`ID`, `google_members`, `facebook_members`, `twitter_members`, `total_members`, `new_members`, `active_today`, `timestamp`, `total_tickets`, `total_assigned_tickets`, `tickets_today`) VALUES
(1, 0, 0, 0, 0, 0, 0, 1601644669, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ipn_log`
--

CREATE TABLE `ipn_log` (
  `ID` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `IP` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ip_block`
--

CREATE TABLE `ip_block` (
  `ID` int(11) NOT NULL,
  `IP` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `reason` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `knowledge_articles`
--

CREATE TABLE `knowledge_articles` (
  `ID` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `userid` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `last_updated_timestamp` int(11) NOT NULL,
  `useful_yes` int(11) NOT NULL,
  `useful_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `knowledge_articles`
--

INSERT INTO `knowledge_articles` (`ID`, `title`, `body`, `userid`, `timestamp`, `catid`, `last_updated_timestamp`, `useful_yes`, `useful_total`) VALUES
(1, 'Install source Moderne', '<p>Sebelum ke tahap proses instalasi source Moderne, terlebih dahulu download dan install yang diperlukan untuk tahap selanjutnya:</p>\r\n\r\n<ul>\r\n	<li>Download dan install Node Js (<a href=\"https://nodejs.org/en/download/\">https://nodejs.org/en/download/</a>)</li>\r\n	<li>Download dan install React Native serta dependecies(<a href=\"https://facebook.github.io/react-native/docs/0.54/getting-started\">https://facebook.github.io/react-native/docs/0.54/getting-started</a>)</li>\r\n	<li>Download Android visual studio (<a href=\"https://developer.android.com/studio\">https://developer.android.com/studio</a>)</li>\r\n</ul>\r\n\r\n<p>Langkah&quot; install source Moderne mengunakan comman prompt (cmd)</p>\r\n\r\n<p>a. Extract (unzip) folder (<strong>Techstore_Shopify-master</strong>)</p>\r\n\r\n<p>b. Buka git bash/terminal (command prompt), pindah ke folder Moderne :</p>\r\n\r\n<p><img alt=\"\" src=\"localhost/moderne/images/gbr/1.1 b.png\" /><a href=\"localhost/moderne/images/gbr/1.1 b.png\"><img alt=\"\" src=\"http://localhost/moderne/images/gbr/11b.png\" /></a></p>\r\n\r\n<p>c. Lakukan proses penginstallan modul pada aplikasi :</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/moderne/images/gbr/11c.png\" /></p>\r\n\r\n<p>d. Jalankan program Aplikasi, menggunakan :</p>\r\n\r\n<ol>\r\n	<li>Android</li>\r\n</ol>\r\n\r\n<p>Pastikan terdapat perangkat (handphone) atau android emulator untuk menjalankan aplikasi. Pada git bash/terminal (command prompt) lakukan perintah:</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/moderne/images/gbr/11d.png\" /></p>\r\n', 1, 1601531127, 1, 1601531127, 0, 0),
(2, 'Mengganti tema shopify untuk mobile app', '<p>a. Download tema shopify Anda.</p>\r\n\r\n<p>&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/21a.png\" /></p>\r\n\r\n<p>b. Extract(unzip) tema shopify Anda dan salin/copy folder <strong>theme TechStore_Shopify-master</strong> (<strong>./Liquid_source/theme)</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/21b.png\" /></p>\r\n\r\n<p>c. Tempel/paste folder<strong> theme</strong> <strong>TechStore_Shopify-master</strong> pada tema shopify Anda yang sudah di download.</p>\r\n\r\n<p>d. Pada folder tema shopify Anda, ubah dalam bentuk format <strong>.zip </strong>dan unggah atau upload pada shopify Anda.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/21d.png\" /></p>\r\n\r\n<p>e. Lakukan publish tema shopify Anda. (Pilihan)</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/21e.png\" /></p>\r\n', 1, 1601531581, 2, 1601620111, 0, 0),
(3, 'Menambah script pada tema shopify', '<p>a. Buka file <strong>scripts.txt</strong> <strong>(./Liquid_source/scripts.txt/) </strong>dan salin/copy pada file<strong> bagian </strong><strong>account.liquid</strong><strong>.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/22a.png\" /></p>\r\n\r\n<p>b. Tempel/paste script ke tema shopify Anda pada file customers/account.liquid lalu klik save atau simpan.</p>\r\n\r\n<p><strong>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/22b.png\" />&nbsp;</strong></p>\r\n\r\n<p>c. Lakukan seperti yang diatas tetapi pada bagian <strong>addresses.liquid</strong><strong>, </strong>copy /salin <strong>bagian script addresses</strong><strong>.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/22c.png\" /></p>\r\n\r\n<p>d. Tempel/paste script ke template shopify Anda pada file customers/addresses.liquid lalu klik save atau simpan.</p>\r\n\r\n<p><strong>&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/22d.png\" />&nbsp;</strong></p>\r\n', 1, 1601531890, 2, 1601620047, 0, 0),
(4, 'Konfigurasi “Domain”', '<p>a. Buka file constants.js (./src/settings/constants.js).</p>\r\n\r\n<p>b. Ubah Domain sebelumnya ke Domain Anda, contoh<strong>:</strong><strong>&nbsp;&nbsp; </strong><a href=\"https://devindoship.myshopify.com\">https://devindoship.myshopify.com</a>.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/31b.png\" /></p>\r\n', 1, 1601532445, 3, 1601621448, 0, 0),
(5, 'Mengubah splash screen', '<p>Ganti gambar splash screen lama dengan gambar splash screen baru (\\Moderne\\img\\splash.jpg).</p>\r\n\r\n<p><strong><img alt=\"\" src=\"http://localhost/moderne/images/gbr/32a.png\" /></strong></p>\r\n', 1, 1601532543, 3, 1601620335, 0, 0),
(6, 'Membuat notifikasi pesan', '<p>a. Buka halaman <a href=\"https://onesignal.com\" target=\"_blank\">https://onesignal.com</a>, lakukan registrasi akun (jika belum punya) dan buat Onesignall app baru.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img src=\"http://localhost/moderne/images/gbr/33a.png\" /></p>\r\n\r\n<p>b. Pilih platform <strong>Google Android, </strong>lalu klik <strong>next </strong>atau selanjutnya.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33b.png\" /></p>\r\n\r\n<p>c. Isi form <strong>firebase server key </strong>dan <strong>firebase sender ID</strong>, untuk dapat melihat keduanya dapat dijelaskan pada langkah selanjutnya.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33c.png\" /></p>\r\n\r\n<p>Untuk dapat melihat <strong>Firebase Server Key </strong>dan <strong>Firebase Sender ID </strong>dapat membuka halaman <a href=\"https://firebase.google.com/\">https://firebase.google.com/</a>.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; a. Buka website <a href=\"https://firebase.google.com/\">https://firebase.google.com/</a>, gunakan alamat email toko Anda lalu klik <strong>Go to console.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33a_fb.png\" /></p>\r\n\r\n<p>&nbsp;&nbsp; b. Buat projek baru atau tambahkan projek baru.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33b_fb.png\" /></p>\r\n\r\n<p>&nbsp;&nbsp; c. Isikan nama projek atau toko Anda lalu klik <strong>Lanjutkan.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33c_fb.png\" /></p>\r\n\r\n<p>&nbsp; d. Pilih <strong>Jangan sekarang </strong>dan klik <strong>Buat project.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33d_fb.png\" /></p>\r\n\r\n<p>&nbsp; e. Pada halaman dashboard, pilih <strong>setting</strong> lalu <strong>setelan proyek.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33e_fb.png\" /></p>\r\n\r\n<p>&nbsp; f. Pilih menu <strong>Cloud Messaging, </strong>lalu salin/copy bagian <strong>kunci server</strong> dan <strong>ID pengirim</strong>.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33f_fb.png\" /></p>\r\n\r\n<p>&nbsp; g. Temple/paste bagian <strong>kunci server</strong> dan <strong>ID pengirim</strong> pada halaman <a href=\"https://onesignal.com\" target=\"_blank\">https://onesignal.com</a> bagian edit app dan klik <strong>save </strong>atau simpan.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33g_fb.png\" /></p>\r\n\r\n<p>&nbsp; h. Buka website <a href=\"https://onesignal.com\" target=\"_blank\">https://onesignal.com</a> bagian setting dapat dilihat <strong>Keys &amp; IDs toko Anda, </strong>salin/copy Onesignal App <strong>ID dan Rest API Key</strong>.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33h_fb.png\" /></p>\r\n\r\n<p>&nbsp;&nbsp; i. Tempel/paste <strong>id </strong>dan <strong>restApiKey</strong> pada file <strong>constants.js</strong> (./<strong>src/settings/constants.js</strong><strong>).</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33i_fb.png\" /></p>\r\n\r\n<p>&nbsp;&nbsp; Untuk membuat notifikasi pesan pada toko Anda, kembali ke website <a href=\"https://onesignal.com\" target=\"_blank\">https://onesignal.com</a> bagian <strong>messages</strong>.</p>\r\n\r\n<p>&nbsp;&nbsp; j. Pada bagian menu <strong>messages, </strong>klik <strong>new push.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33j_fb.png\" /></p>\r\n\r\n<p>&nbsp;&nbsp; k. Tulis pesan sesuai keinginan untuk notifikasi aplikasi Anda pada bagian <strong>Title </strong>dan <strong>Messages.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33k_fb.png\" /></p>\r\n\r\n<p><strong>&nbsp;&nbsp; l. (Optional) </strong>Jika pesan notifikasi terdapat gambar , LED dll, lakukan pengisian pada gambar dibawah ini:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/33l_fb.png\" /></p>\r\n\r\n<p>m. Setelah selesai mengisi pesan notifikasi, klik <strong>confirm.</strong></p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/moderne/images/gbr/33m_fb.png\" />&nbsp;.</p>\r\n', 1, 1601533732, 3, 1601622672, 0, 0),
(7, 'Mengganti nama Apps', '<p>a. Ubah folder lama ke folder baru pada folder .<strong>\\Moderne\\android\\app\\src\\main\\java\\com</strong>, contoh: folder moderne ke penta. (Catatan: aturan penamaan folder tidak boleh spasi)</p>\r\n\r\n<p>b. Ganti nama apps lama (moderne) dengan apps baru (penta), pada 6 file diantaranya:</p>\r\n\r\n<ul>\r\n	<li><strong>.\\Moderne\\android\\app\\build.gradle:</strong></li>\r\n	<li><strong>&nbsp;.\\Moderne\\android\\app\\BUCK:</strong></li>\r\n	<li><strong>.\\Moderne\\android\\app\\build\\generated\\source\\buildConfig\\debug\\com\\moderne\\BuildConfig.java:</strong></li>\r\n	<li><strong>.\\Moderne\\android\\app\\build\\generated\\source\\r\\debug\\com\\moderne\\Manifest.java:</strong></li>\r\n	<li><strong>.\\Moderne\\android\\settings.gradle</strong></li>\r\n	<li><strong>.\\Moderne\\package.json</strong></li>\r\n	<li><strong>.\\Moderne\\package-lock.json</strong></li>\r\n	<li><strong>.\\Moderne\\app.json</strong></li>\r\n	<li><strong>.\\Moderne\\android\\app\\build\\generated\\source\\buildConfig\\debug\\com\\moderne\\R.java:</strong></li>\r\n</ul>\r\n\r\n<p>Berikut merupakan salah satu gambar untuk mengganti nama aplikasi:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/34b.png\" /></p>\r\n\r\n<p>c. Pada console jalankan perintah</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/34c.png\" /></p>\r\n\r\n<p>d. Lalu jalankan apps pada console</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/34d.png\" /></p>\r\n', 1, 1601533962, 3, 1601620580, 0, 0),
(8, 'Mengganti icon launcher', '<p>a. Mengganti&nbsp;dan mengatur Icon launcher lama dengan Icon laucher baru, buka website <a href=\"https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html\">https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html</a>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Setting Icon dapat menggunakan text, Icon atau image.</li>\r\n</ul>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/35a.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Setting versi square</strong> terlebih dahulu, setelah selesai beri nama <strong>ic_launcher </strong>dan download filenya.</li>\r\n</ul>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/35a_1.png\" /></p>\r\n\r\n<ul>\r\n	<li><strong>Setting versi circle</strong>, setelah selesai beri nama <strong>ic_launcher_round</strong> dan download filenya.</li>\r\n</ul>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/35a_2.png\" /></p>\r\n\r\n<p>b. Pada folder <strong>.\\Moderne\\android\\app\\src\\main</strong>, hapus folder <strong>ic_launcher.zip </strong>versi sebelumnya.</p>\r\n\r\n<p>c. Salin/copy dan temple/paste yang telah di download yaitu <strong>ic_launcher.zip</strong> dan <strong>ic_launcher_round.zip</strong> pada folder <strong>.\\android\\app\\src\\main\\, </strong>lalu extract kedua folder tersebut.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/35c.png\" /></p>\r\n\r\n<p>d. Cek semua folder res (<strong>.\\Moderne\\android\\app\\src\\main\\res</strong>).</p>\r\n\r\n<p>e. Jika semua folder <strong>mipmap</strong> masih terdapat gambar yang lama, maka salin/copy semua filenya dari folder <strong>mipmap-hdpi</strong> dan paste/tempel pada folder <strong>mipmap</strong>.</p>\r\n', 1, 1601534387, 3, 1601620807, 0, 0),
(9, 'Mengubah warna App', '<p>a. Buka toko shopify Anda, pilih themes lalu klik customize.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/41a.png\" />&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>b. Pada theme settings bagian App Colors.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/41b.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>c. Ganti warna app Anda, contoh: warna button, header dll, setelah ubah warna sesuai keinginan lalu klik save atau simpan.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/41c.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>d. Pada perangkat (handphone) atau emulator, pilih option developer tools (dengan digoyangkan hp) dan klik <strong>Reload </strong>untuk mengubah warna yang sudah diatur sebelumnya.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/41d.jpg\" /></p>\r\n', 1, 1601534834, 4, 1601622114, 0, 0),
(10, 'Membuat menu app (Menu side &amp; menu horizon)', '<p>a. Pada <strong>Theme settings </strong>bagian <strong>App &amp;ndash; Side menu </strong>atau <strong>App &amp;ndash; Menu horizon</strong>, langkah membuat menu side dan menu horizon sama tetapi dalam hal ini hanya dijelaskan membuat menu side.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/42a.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>b. Centang <strong>use side menu </strong>logo, lalu pilih <strong>choose side menu </strong>sesuai dengan menu App Anda yang sudah dibuat sebelumnya<strong>.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/42b.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>c. Setelah pilih menu yang sesuai, lalu klik <strong>select </strong>dan klik <strong>save</strong> atau simpan.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/42c.png\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>d. Pada device (handphone) atau emulator, pilih option developer tools (dengan digoyangkan hp) dan klik <strong>Reload </strong>untuk menampilkan pilihan menu.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/42d.jpg\" /></p>\r\n', 1, 1601535121, 4, 1601622344, 0, 0),
(11, 'Menambah slider dan banner', '<p>Menambah slider dan banner dilakukan dengan langkah yang sama tetapi dalam hal ini hanya dijelaskan langkah mengubah banner:</p>\r\n\r\n<p>a. Pada <strong>Theme settings </strong>bagian <strong>App-Home page-Banner.</strong></p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/43a.png\" /></p>\r\n\r\n<p>b. Centang <strong>use banner </strong>dan isi sesuai dengan banner apps Anda dan klik <strong>save</strong> atau simpan.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/43b.png\" /></p>\r\n\r\n<p>c. Pada device (handphone) atau emulator, pilih option developer tools (dengan digoyangkan hp) dan klik <strong>Reload </strong>untuk menambah banner yang sudah diatur sebelumnya.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/43c.jpg\" /></p>\r\n', 1, 1601535391, 4, 1601622245, 0, 0),
(12, 'Menambah collection', '<p>a. Pada <strong>Theme settings </strong>bagian <strong>App-Home page-Collection.</strong></p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/moderne/images/gbr/44a.png\" /></p>\r\n\r\n<p>b. Centang <strong>use collection </strong>dan isi sesuai dengan collection yang diinginkan.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/moderne/images/gbr/44b.png\" /></p>\r\n\r\n<p>c. Pada <strong>Choose collection, </strong>pilih collection yang Anda inginkan dan sudah dibuat sebelumnya lalu klik <strong>select </strong>dan klik <strong>save </strong>atau simpan.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/moderne/images/gbr/44c.png\" /></p>\r\n', 1, 1601535554, 4, 1601535554, 0, 0),
(13, 'Menambah lokasi toko', '<p>a. Pada <strong>Theme settings </strong>bagian menu <strong>App-Store locator</strong></p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img alt=\"\" src=\"http://localhost/moderne/images/gbr/45a.png\" /></p>\r\n\r\n<p>b. Centang <strong>show stores locator dan show stores</strong>, lalu isikan nama toko, deskripsi toko dan titik koordinat toko.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt=\"\" src=\"http://localhost/moderne/images/gbr/45b.png\" /></p>\r\n\r\n<p>Menentukan titik koordinat toko dapat menggunakan google maps dan dapat diambil titik koordinat toko, berikut dapat dijelaskan dibawah ini:</p>\r\n\r\n<ol>\r\n	<li>Isi alamat toko Anda dan klik kanan lalu pilih <strong>what&amp;rsquo;s here?</strong></li>\r\n</ol>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt=\"\" src=\"http://localhost/moderne/images/gbr/45b_1.png\" /></p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; 2. Kemudian terdapat titik koordinat toko, lalu salin titik koodinat pada google maps.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img alt=\"\" src=\"http://localhost/moderne/images/gbr/45b_2.png\" /></p>\r\n\r\n<p>c. Tempel/paste titik koordinat lokasi toko Anda dan klik <strong>save </strong>atau simpan.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img alt=\"\" src=\"http://localhost/moderne/images/gbr/45c_2.png\" /></p>\r\n', 1, 1601535980, 4, 1601535980, 0, 0),
(14, 'Generate release apk', '<p>a. Unistall aplikasi mobile di android</p>\r\n\r\n<p>b. Buka git bash/terminal dan jalankan perintah:</p>\r\n\r\n<p>&nbsp; &nbsp; <img alt=\"\" src=\"http://localhost/moderne/images/gbr/5b.png\" /></p>\r\n\r\n<p>c. Jika aplikasi berhasil dijalankan, lakukan test (dengan digoyangkan hp) dan apabila tidak muncul tampilan developer tools (reload, dll), maka sudah berhasil.</p>\r\n', 1, 1601536065, 5, 1601536065, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `knowledge_categories`
--

CREATE TABLE `knowledge_categories` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `knowledge_categories`
--

INSERT INTO `knowledge_categories` (`ID`, `name`, `description`, `image`, `parent_category`) VALUES
(1, 'Pre Instalasi', '', '59b430dd94f8c2885d37aabf6694ad92.png', 0),
(2, 'Integrasi app modern dengan tema shopify', '', 'ad9498412c3789097069ce6d601e5a69.png', 0),
(3, 'Konfigurasi Moderne', '', '33b0786a2eba892983148e95746550c7.png', 0),
(4, 'Pengaturan tampilan pada mobile app', '', '58717cc001a30161bdfe8f7d31c24118.png', 0),
(5, 'Generate release apk', '', '05d1cf1762bf5268fcf26abc70fd9d6e.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `knowledge_groups`
--

CREATE TABLE `knowledge_groups` (
  `ID` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `knowledge_votes`
--

CREATE TABLE `knowledge_votes` (
  `ID` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `IP` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `ID` int(11) NOT NULL,
  `IP` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT 0,
  `timestamp` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `ID` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT 0,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `IP` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_logs`
--

CREATE TABLE `payment_logs` (
  `ID` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT 0,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `email` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `processor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_plans`
--

CREATE TABLE `payment_plans` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `hexcolor` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fontcolor` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cost` decimal(10,2) NOT NULL DEFAULT 0.00,
  `days` int(11) NOT NULL DEFAULT 0,
  `sales` int(11) NOT NULL DEFAULT 0,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_plans`
--

INSERT INTO `payment_plans` (`ID`, `name`, `hexcolor`, `fontcolor`, `cost`, `days`, `sales`, `description`, `icon`) VALUES
(2, 'BASIC', '65E0EB', 'FFFFFF', '3.00', 30, 11, 'This is the basic plan which gives you a introduction to our Premium Plans', 'glyphicon glyphicon-heart-empty'),
(3, 'Professional', '55CD7B', 'FFFFFF', '7.99', 90, 3, 'Get all the benefits of basic at a cheaper price and gain content for longer.', 'glyphicon glyphicon-piggy-bank'),
(4, 'LIFETIME', 'EE5782', 'FFFFFF', '300.00', 0, 2, 'Become a premium member for life and have access to all our premium content.', 'glyphicon glyphicon-gift');

-- --------------------------------------------------------

--
-- Table structure for table `reset_log`
--

CREATE TABLE `reset_log` (
  `ID` int(11) NOT NULL,
  `IP` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_layouts`
--

CREATE TABLE `site_layouts` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout_path` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_layouts`
--

INSERT INTO `site_layouts` (`ID`, `name`, `layout_path`) VALUES
(1, 'Basic', 'layout/themes/layout.php'),
(2, 'Titan', 'layout/themes/titan_layout.php'),
(3, 'Dark Fire', 'layout/themes/dark_fire_layout.php'),
(4, 'Light Blue', 'layout/themes/light_blue_layout.php');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `ID` int(11) NOT NULL,
  `site_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `site_desc` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upload_path` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `upload_path_relative` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `site_email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'logo.png',
  `register` int(11) NOT NULL,
  `disable_captcha` int(11) NOT NULL,
  `date_format` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `avatar_upload` int(11) NOT NULL DEFAULT 1,
  `file_types` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_consumer_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_consumer_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `disable_social_login` int(11) NOT NULL,
  `facebook_app_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_app_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_client_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `paypal_email` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `paypal_currency` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `payment_enabled` int(11) NOT NULL,
  `payment_symbol` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '$',
  `global_premium` int(11) NOT NULL,
  `install` int(11) NOT NULL DEFAULT 1,
  `login_protect` int(11) NOT NULL,
  `activate_account` int(11) NOT NULL,
  `default_user_role` int(11) NOT NULL,
  `secure_login` int(11) NOT NULL,
  `stripe_secret_key` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_publish_key` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `enable_ticket_uploads` int(11) NOT NULL,
  `enable_ticket_guests` int(11) NOT NULL,
  `enable_ticket_edit` int(11) NOT NULL,
  `require_login` int(11) NOT NULL,
  `protocol` int(11) NOT NULL,
  `protocol_path` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `protocol_email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `protocol_password` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ticket_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `protocol_ssl` int(11) NOT NULL,
  `ticket_rating` int(11) NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `google_recaptcha` int(11) NOT NULL,
  `google_recaptcha_secret` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `google_recaptcha_key` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `logo_option` int(11) NOT NULL,
  `avatar_height` int(11) NOT NULL,
  `avatar_width` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `checkout2_accountno` int(11) NOT NULL,
  `checkout2_secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'layout/themes/titan_layout.php',
  `imap_ticket_string` varchar(500) COLLATE utf8_unicode_ci DEFAULT '## Ticket ID: ',
  `imap_reply_string` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '## - REPLY ABOVE THIS LINE - ##',
  `captcha_ticket` int(11) NOT NULL,
  `envato_personal_token` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cache_time` int(11) NOT NULL DEFAULT 3600,
  `ticket_note_close` int(11) NOT NULL,
  `default_status` int(11) NOT NULL,
  `close_ticket_reply` int(11) NOT NULL,
  `disable_cert` int(11) NOT NULL,
  `staff_status` int(11) NOT NULL,
  `client_status` int(11) NOT NULL,
  `public_tickets` int(11) NOT NULL,
  `enable_knowledge` int(11) NOT NULL DEFAULT 1,
  `enable_faq` int(11) NOT NULL DEFAULT 1,
  `logo_height` int(11) NOT NULL DEFAULT 32,
  `logo_width` int(11) NOT NULL DEFAULT 123,
  `enable_documentation` int(11) NOT NULL,
  `resize_avatar` int(11) NOT NULL,
  `alert_users` varchar(1500) COLLATE utf8_unicode_ci NOT NULL,
  `price_per_ticket` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`ID`, `site_name`, `site_desc`, `upload_path`, `upload_path_relative`, `site_email`, `site_logo`, `register`, `disable_captcha`, `date_format`, `avatar_upload`, `file_types`, `twitter_consumer_key`, `twitter_consumer_secret`, `disable_social_login`, `facebook_app_id`, `facebook_app_secret`, `google_client_id`, `google_client_secret`, `file_size`, `paypal_email`, `paypal_currency`, `payment_enabled`, `payment_symbol`, `global_premium`, `install`, `login_protect`, `activate_account`, `default_user_role`, `secure_login`, `stripe_secret_key`, `stripe_publish_key`, `enable_ticket_uploads`, `enable_ticket_guests`, `enable_ticket_edit`, `require_login`, `protocol`, `protocol_path`, `protocol_email`, `protocol_password`, `ticket_title`, `protocol_ssl`, `ticket_rating`, `notes`, `google_recaptcha`, `google_recaptcha_secret`, `google_recaptcha_key`, `logo_option`, `avatar_height`, `avatar_width`, `default_category`, `checkout2_accountno`, `checkout2_secret`, `layout`, `imap_ticket_string`, `imap_reply_string`, `captcha_ticket`, `envato_personal_token`, `cache_time`, `ticket_note_close`, `default_status`, `close_ticket_reply`, `disable_cert`, `staff_status`, `client_status`, `public_tickets`, `enable_knowledge`, `enable_faq`, `logo_height`, `logo_width`, `enable_documentation`, `resize_avatar`, `alert_users`, `price_per_ticket`) VALUES
(1, 'Moderne', 'Welcome to Moderne', 'C:\\xampp\\htdocs\\moderne\\uploads', 'uploads', 'test@test.com', 'logo.png', 0, 1, 'd/m/Y h:i', 1, 'gif|png|jpg|jpeg', '', '', 0, '', '', '', '', 1028, '', 'USD', 1, '$', 0, 0, 1, 0, 10, 0, '', '', 1, 1, 1, 0, 1, 'imap.gmail.com:993', '', '', 'Support Ticket', 1, 1, '', 0, '', '', 0, 100, 100, 1, 0, '', 'layout/themes/titan_layout.php', '## Ticket ID:', '## - REPLY ABOVE THIS LINE - ##', 0, '', 3600, 0, 1, 1, 0, 2, 1, 1, 1, 1, 32, 123, 1, 1, '', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `ID` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `userid` int(11) NOT NULL,
  `assignedid` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `last_reply_timestamp` int(11) NOT NULL,
  `last_reply_userid` int(11) NOT NULL,
  `notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `message_id_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guest_email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `guest_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_reply_string` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `ticket_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `close_ticket_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `archived` int(11) NOT NULL,
  `public` int(11) NOT NULL,
  `close_timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_categories`
--

CREATE TABLE `ticket_categories` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cat_parent` int(11) NOT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `no_tickets` int(11) NOT NULL,
  `auto_assign` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ticket_categories`
--

INSERT INTO `ticket_categories` (`ID`, `name`, `description`, `cat_parent`, `image`, `no_tickets`, `auto_assign`) VALUES
(1, 'Default', '<p>The default group.</p>\r\n', 0, '8ee11fb7b8cfe92cdf20749847761694.png', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_category_groups`
--

CREATE TABLE `ticket_category_groups` (
  `ID` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `catid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_custom_fields`
--

CREATE TABLE `ticket_custom_fields` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `options` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `help_text` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `required` int(11) NOT NULL,
  `all_cats` int(11) NOT NULL,
  `hide_clientside` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_custom_field_cats`
--

CREATE TABLE `ticket_custom_field_cats` (
  `ID` int(11) NOT NULL,
  `fieldid` int(11) NOT NULL,
  `catid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_files`
--

CREATE TABLE `ticket_files` (
  `ID` int(11) NOT NULL,
  `ticketid` int(11) NOT NULL,
  `upload_file_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `replyid` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_history`
--

CREATE TABLE `ticket_history` (
  `ID` int(11) NOT NULL,
  `ticketid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `message` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_replies`
--

CREATE TABLE `ticket_replies` (
  `ID` int(11) NOT NULL,
  `ticketid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `replyid` int(11) NOT NULL,
  `files` int(11) NOT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_user_custom_fields`
--

CREATE TABLE `ticket_user_custom_fields` (
  `ID` int(11) NOT NULL,
  `fieldid` int(11) NOT NULL,
  `ticketid` int(11) NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `itemname` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `support` int(11) NOT NULL,
  `error` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `IP` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `avatar` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.png',
  `joined` int(11) NOT NULL DEFAULT 0,
  `joined_date` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `online_timestamp` int(11) NOT NULL DEFAULT 0,
  `oauth_provider` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oauth_id` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oauth_token` varchar(1500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oauth_secret` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email_notification` int(11) NOT NULL DEFAULT 1,
  `aboutme` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `points` decimal(10,2) NOT NULL DEFAULT 0.00,
  `premium_time` int(11) NOT NULL DEFAULT 0,
  `user_role` int(11) NOT NULL DEFAULT 0,
  `premium_planid` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1,
  `activate_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `noti_count` int(11) NOT NULL,
  `custom_view` int(11) NOT NULL,
  `active_project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `email`, `password`, `token`, `IP`, `username`, `first_name`, `last_name`, `avatar`, `joined`, `joined_date`, `online_timestamp`, `oauth_provider`, `oauth_id`, `oauth_token`, `oauth_secret`, `email_notification`, `aboutme`, `points`, `premium_time`, `user_role`, `premium_planid`, `active`, `activate_code`, `noti_count`, `custom_view`, `active_project`) VALUES
(1, 'adminmoderne@mail.com', '$2a$12$sY/UD/3ykAixYh91lneHQeWG2F707fwGTmREg9rMF1HM8Z8ihwHk6', '6d3446e40ca6dca3e012d0d140bcd2db', '::1', 'admin_moderne', 'Admin', 'User', 'default.png', 1601430811, '9-2020', 1601644606, '', '', '', '', 1, '', '0.00', 0, 1, 0, 1, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_custom_fields`
--

CREATE TABLE `user_custom_fields` (
  `ID` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `fieldid` int(11) NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_events`
--

CREATE TABLE `user_events` (
  `ID` int(11) NOT NULL,
  `IP` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `event` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `ID` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `default` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`ID`, `name`, `default`) VALUES
(1, 'Default Group', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_group_users`
--

CREATE TABLE `user_group_users` (
  `ID` int(11) NOT NULL,
  `groupid` int(11) NOT NULL DEFAULT 0,
  `userid` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `ID` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `url` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `message` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `fromid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `admin` int(11) NOT NULL DEFAULT 0,
  `admin_settings` int(11) NOT NULL DEFAULT 0,
  `admin_members` int(11) NOT NULL DEFAULT 0,
  `admin_payment` int(11) NOT NULL DEFAULT 0,
  `admin_announcements` int(11) NOT NULL,
  `banned` int(11) NOT NULL,
  `ticket_manager` int(11) NOT NULL,
  `ticket_worker` int(11) NOT NULL,
  `knowledge_manager` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `faq_manager` int(11) NOT NULL,
  `documentation_manager` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`ID`, `name`, `admin`, `admin_settings`, `admin_members`, `admin_payment`, `admin_announcements`, `banned`, `ticket_manager`, `ticket_worker`, `knowledge_manager`, `client`, `faq_manager`, `documentation_manager`) VALUES
(1, 'Admin', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 'Member Manager', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 'Admin Settings', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 'Admin Payments', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 'Member', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 'Banned', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(7, 'Ticket Manager', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
(8, 'Ticket Worker', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
(9, 'Knowledge Manager', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0),
(10, 'Client', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(11, 'FAQ Manager', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_role_permissions`
--

CREATE TABLE `user_role_permissions` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `classname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hook` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_role_permissions`
--

INSERT INTO `user_role_permissions` (`ID`, `name`, `description`, `classname`, `hook`) VALUES
(1, 'ctn_308', 'ctn_308', 'admin', 'admin'),
(2, 'ctn_309', 'ctn_309', 'admin', 'admin_settings'),
(3, 'ctn_310', 'ctn_310', 'admin', 'admin_members'),
(4, 'ctn_311', 'ctn_311', 'admin', 'admin_payment'),
(5, 'ctn_33', 'ctn_33', 'banned', 'banned'),
(6, 'ctn_397', 'ctn_398', 'ticket', 'ticket_manager'),
(7, 'ctn_399', 'ctn_400', 'ticket', 'ticket_worker'),
(8, 'ctn_401', 'ctn_402', 'knowledge', 'knowledge_manager'),
(9, 'ctn_403', 'ctn_404', 'client', 'client'),
(11, 'ctn_744', 'ctn_745', 'knowledge', 'faq_manager'),
(12, 'ctn_806', 'ctn_807', 'knowledge', 'documentation_manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `canned_responses`
--
ALTER TABLE `canned_responses`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `custom_statuses`
--
ALTER TABLE `custom_statuses`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `custom_views`
--
ALTER TABLE `custom_views`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `documentation_projects`
--
ALTER TABLE `documentation_projects`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `document_files`
--
ALTER TABLE `document_files`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `home_stats`
--
ALTER TABLE `home_stats`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ipn_log`
--
ALTER TABLE `ipn_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ip_block`
--
ALTER TABLE `ip_block`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `knowledge_articles`
--
ALTER TABLE `knowledge_articles`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `knowledge_categories`
--
ALTER TABLE `knowledge_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `knowledge_groups`
--
ALTER TABLE `knowledge_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `knowledge_votes`
--
ALTER TABLE `knowledge_votes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `payment_logs`
--
ALTER TABLE `payment_logs`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `payment_plans`
--
ALTER TABLE `payment_plans`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `reset_log`
--
ALTER TABLE `reset_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `site_layouts`
--
ALTER TABLE `site_layouts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_categories`
--
ALTER TABLE `ticket_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_category_groups`
--
ALTER TABLE `ticket_category_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_custom_fields`
--
ALTER TABLE `ticket_custom_fields`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_custom_field_cats`
--
ALTER TABLE `ticket_custom_field_cats`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_files`
--
ALTER TABLE `ticket_files`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_history`
--
ALTER TABLE `ticket_history`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_replies`
--
ALTER TABLE `ticket_replies`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ticket_user_custom_fields`
--
ALTER TABLE `ticket_user_custom_fields`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_custom_fields`
--
ALTER TABLE `user_custom_fields`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_events`
--
ALTER TABLE `user_events`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_group_users`
--
ALTER TABLE `user_group_users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_role_permissions`
--
ALTER TABLE `user_role_permissions`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `canned_responses`
--
ALTER TABLE `canned_responses`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_statuses`
--
ALTER TABLE `custom_statuses`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `custom_views`
--
ALTER TABLE `custom_views`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documentation_projects`
--
ALTER TABLE `documentation_projects`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_files`
--
ALTER TABLE `document_files`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_stats`
--
ALTER TABLE `home_stats`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ipn_log`
--
ALTER TABLE `ipn_log`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip_block`
--
ALTER TABLE `ip_block`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `knowledge_articles`
--
ALTER TABLE `knowledge_articles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `knowledge_categories`
--
ALTER TABLE `knowledge_categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `knowledge_groups`
--
ALTER TABLE `knowledge_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `knowledge_votes`
--
ALTER TABLE `knowledge_votes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_logs`
--
ALTER TABLE `payment_logs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_plans`
--
ALTER TABLE `payment_plans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reset_log`
--
ALTER TABLE `reset_log`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_layouts`
--
ALTER TABLE `site_layouts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_categories`
--
ALTER TABLE `ticket_categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ticket_category_groups`
--
ALTER TABLE `ticket_category_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_custom_fields`
--
ALTER TABLE `ticket_custom_fields`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_custom_field_cats`
--
ALTER TABLE `ticket_custom_field_cats`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_files`
--
ALTER TABLE `ticket_files`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_history`
--
ALTER TABLE `ticket_history`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_replies`
--
ALTER TABLE `ticket_replies`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_user_custom_fields`
--
ALTER TABLE `ticket_user_custom_fields`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_custom_fields`
--
ALTER TABLE `user_custom_fields`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_events`
--
ALTER TABLE `user_events`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_group_users`
--
ALTER TABLE `user_group_users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_role_permissions`
--
ALTER TABLE `user_role_permissions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
